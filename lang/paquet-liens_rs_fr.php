<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// L
	'liens_rs_description' => 'Renseigner les liens vers vos réseaux sociaux pour les rendre disponibles sur le site public',
	'liens_rs_nom' => 'Liens vers les réseaux sociaux',
	'liens_rs_slogan' => '',
);