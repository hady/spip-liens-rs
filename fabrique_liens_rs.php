<?php

/**
 *  Fichier généré par la Fabrique de plugin v6
 *   le 2017-05-11 17:57:38
 *
 *  Ce fichier de sauvegarde peut servir à recréer
 *  votre plugin avec le plugin «Fabrique» qui a servi à le créer.
 *
 *  Bien évidemment, les modifications apportées ultérieurement
 *  par vos soins dans le code de ce plugin généré
 *  NE SERONT PAS connues du plugin «Fabrique» et ne pourront pas
 *  être recréées par lui !
 *
 *  La «Fabrique» ne pourra que régénerer le code de base du plugin
 *  avec les informations dont il dispose.
 *
**/

if (!defined("_ECRIRE_INC_VERSION")) return;

$data = array (
  'fabrique' => 
  array (
    'version' => 6,
  ),
  'paquet' => 
  array (
    'prefixe' => 'liens_rs',
    'nom' => 'Liens vers les réseaux sociaux',
    'slogan' => '',
    'description' => '',
    'version' => '1.0.0',
    'auteur' => 'Hadrien',
    'auteur_lien' => '',
    'licence' => 'GNU/GPL',
    'categorie' => 'communication',
    'etat' => 'dev',
    'compatibilite' => '[3.1.0;3.1.*]',
    'documentation' => '',
    'administrations' => '',
    'schema' => '1.0.0',
    'formulaire_config' => 'on',
    'formulaire_config_titre' => 'Configuration des liens vers les réseaux sociaux',
    'fichiers' => 
    array (
      0 => 'autorisations',
    ),
    'inserer' => 
    array (
      'paquet' => '',
      'administrations' => 
      array (
        'maj' => '',
        'desinstallation' => '',
        'fin' => '',
      ),
      'base' => 
      array (
        'tables' => 
        array (
          'fin' => '',
        ),
      ),
    ),
    'scripts' => 
    array (
      'pre_copie' => '',
      'post_creation' => '',
    ),
    'exemples' => '',
  ),
  'objets' => 
  array (
  ),
  'images' => 
  array (
    'paquet' => 
    array (
      'logo' => 
      array (
        0 => 
        array (
          'extension' => 'png',
          'contenu' => 'iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAB00lEQVR4nO2b0a2DMAxFzyjeqGzQbtRu0GwEG9BN6EfJe1H1qkcTJ8QJR/JPBZLvJQbiGmiHM3AHRmBZY1x/O+2YV3YGYOZX9KeY12Ob4sr/wt/jukumGbjxvXgftx3yVWUgXrwP0+Wwpea33BNMciFdvOlV4NAzwBXNXInwOZ8aY+HcVdAS78McXRsg6IqfimafgPB6p9e++q6chDiEPMJ9VPsYFPIKX4BHIS1fIeQXXuXVF8oJX6hoMyTECXfruTE7wirEC2nCQwZe9byl5ndf9oKe8HeG9bgpOG9af2taeNUIhoWfie+yCoaFp3RZBcPCIb7LKhgXDmldVtPCQafLalK4Z0vNNykcdLuspoR7HJ0K92h2WRcMCfdoX31zdG9AuHvqsgQc+qvgjiEjLugbYM6IB/lMMGFEqVfhqo0ouRmq1ojYLqsQdzOt0oiULqvQmBGOuC6r0JARKQiHEcBhxA9CPiNOGJoVFvSMMD0rLKQZ0cyssJBnU/YpqviH+C+EckZUVw4hQn4j5kJakhDyGlH1KggR8nWvTSHoGnDMChfOXYXuDdDsYpuZFQ5xdHwThOOTGUCni/0onLMq3X82B51/OOkxOyusialZ4ZxEdbGfeacJTJ5xtPoAAAAASUVORK5CYII=',
        ),
      ),
    ),
    'objets' => 
    array (
    ),
  ),
);